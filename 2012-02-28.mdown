#System and Device Programming#
##28 February 2012 – Theory Solutions##

###1
Write the sequence of system calls that allows a process to catch the first received **SIGUSR1** signal without aborting, ignore the second **SIGUSR1** signal that is received after the first one, and continue with the same behaviour (catch – ignore) for the successive **SIGUSR1** signals.

*(answer by M. Ardizzone)*

    void catchSignals(){
      while(1){
        signal(SIGUSR1,printCatch(sig));  
        pause();  //A call to kill() will be made and the process will catch it
        //It's bad practice to use SIG_IGN because it permanetly ignore the other signals
        //For reference: http://www.gnu.org/software/libc/manual/html_node/Basic-Signal-Handling.html
        //In particular:
        /*
          "If you set the action for a signal to SIG_IGN, or if you set it to SIG_DFL and the default action is to ignore that signal, then any pending signals of that type are discarded (even if they are blocked).
          Discarding the pending signals means that they will never be delivered, not even if you subsequently specify another action and unblock this kind of signal."
        */
        //So we use another handler
        signal(SIGUSR1,ign(sig));
        pause(); //The kill will be caught but nothing will be done
      }
    }

    void printCatch(int sig){
      printf("Signal caught\n");
    }

    void ign(int sig){
      //Do nothing
    }

*(answer by G. David)*

    void hand2(int s);
    void hand1(int s);

    void hand1(int s) {
    	printf("Catch\n");
    	signal(SIGUSR1, hand2);
    }
    void hand2(int s) {
    	signal(SIGUSR1, hand1);
    }

    int main() {
    	signal(SIGUSR1, hand1);
    	while (1)
    		pause();
    }

###2
Illustrate the syntax used to mount a file system /dev/fsx on a directory dir. Every user is enabled to mount a file-system? Which kernel data structures are affected by the mounting operation? How?

*(answer by M. Ardizzone)*

  1. Make the directory for mount point: `mkdir dir`
  2. Make it your own: `chown myuser:mygroup dir`
  3. Mount the filesystem: `mount /dev/fsx dir`

Generally only the superuser is able to perform a mount, unless the superuser has granted special privileges to other users.

When a mount is performed a new entry is added to the global mount table which references the corresponding inode in the inode table. The inode will be updated to indicate the mount point (with a flag) and the reference count will be updated as well.

***(NOT SURE, TRYING TO GIVE MEANING TO THE MEANINGLESS PICTURE FOUND ON SLIDE 20 OF FILESYSTEM3.PDF)***

*(answer by G. David)*

    mount /dev/fsx dir
The users enabled to mount a device are the root user and all users belonging to the same group of the device ([ref](http://linux.die.net/man/8/mount): group option).  
Mounting a device involves two system tables, the inode table and the mount table.  
The inode table is the table that contains all referenced inodes in the system.  
When a device *dev* is mounted on a directory *dir*, the inode entry corresponding to *dir* will be marked as mount point. Moreover a new inode entry will be created for the root inode of the filesystem of *dev*.  
Also a new entry is added to the mount table that contains pointers to the inode on which *dev* is mounted and the root inode of *dev*.
In this way, when *dir* is referenced while *dev* is mounted, the system can understand that *dev* is mounted on *dir* and it can retrieve its root inode.

###3
Implement this precedence graph with the minimum number of semaphores. The processes are 5, and they are cyclic. In every cycle either P2 or P4, not both, is executed in concurrency with P3.

*(answer by M. Ardizzone)*

**Main**

    init(S1,1);
    init(M1,0);
    init(S3,0);
    init(S5,0);

**Process 1:**

    while(1){
      W(S1)
      S(M1)
      S(S3);
    }

**Process 2:**

    while(1){
      W(M1);
      S(S5);
    }
**Process 3:**

    while(1){
      W(S3);
      S(S5);
    }

**Process 4:**

    while(1){
      W(M1);
      S(S5);
    }

**Process 5:**

    while(1){
      W(S5);
      W(S5);
      S(S1);
    }

###4
Describe why the C library is not thread safe, and outline the main difference between the _beginthreadex and the CreateThread functions. Show why a program using CreateThread could be erroneous.

*(answer by M. Ardizzone)*

C library is not safe because there are some functions that make use of global variables: these variables are needed because not all the required information for the function can be passed by parameter.

Some string manipulation functions might use global variables. 

Since they use global variables, which are shared among threads, this will surely create issues and provide results different from the expected one.

The issue was solved by providing an alternative implementation of such functions which require an overhead: this overhead is initialized by creating the thread through `_beginthreadex`. Apart from that, `_beginthreadex` adn `CreateThread` have exactly the same parameters.

*(answer by O. Scicolone)*
C library is not thread safe because there are some functions (ex. strtok) that use global variables so in single thread programming there are no problems.
Instead with MT programming we want that these global vars are copied locally into thread local storage.
_beginthreadex try to transform C library in a thread safe form.
So, for use it we need to:
  - cast _beginthreadex to (HANDLE)
  - use _endthreadex instead of ExitThread
  - include <process.h>
  - define _MT before windows.h
  - Link with LIBCMT.LIB

###5
Which are the roles of files pointers and of the overlapped structures in direct file access on WIN32 systems. Briefly describe common aspects and differences. Provide a brief example of application for both of them.

  + File pointers are used to sequentially move inside a file. When a read/write occurs on a file, the pointer is automatically advanced to the next record. The pointer can be manually repositioned through `SetFilePointer` to the desired position in the file.
  + Overlapped structures were made for asynchronous IO, but they can be used as a substitute of file pointers. The main difference is that when a read/write call uses an OV structure, the file pointer is not automatically advanced to the next position: the programmer will have to properly update the fields `Offset` and `OffsetHigh` to make the OV structure point to the next record in the file. If no action is taken, read/write IO will continue to read/write in the same position.

Here is an example with file pointers:

    /*
      The following function reads numbers from a binary file and if the number is lower
      than 0, it is put equal 0
    */
    VOID updateNumberFilepointers(HANDLE fh){
      DWORD n;
      DWORD bR, bW;
      LARGE_INTEGER postion;
      position.QuadPart = (-sizeof(DWORD));
      while(!ReadFile(fh,&n,sizeof(DWORD),&bR,NULL) && bR > 0){
        if(n < 0){
          n = 0;
          SetFilePointer(fh,position.LowPart,position.HighPart,1); //Go back by one record
          WriteFile(fh,&n,sizeof(DWORD),&bW,NULL);
        }
      }
    }

Here is an example with overlapped structures:

    /*
      The following function reads numbers from a binary file and if the number is lower
      than 0, it is put equal 0
    */
    VOID updateNumberOverlapped(HANDLE fh){
      DWORD n;
      DWORD bR, bW;
      OVERLAPPED ov = {0,0,0,0,NULL};
      LARGE_INTEGER postion;
      position.QuadPart = 0;
      
      while(!ReadFile(fh,&n,sizeof(DWORD),&bR,&ov) && bR > 0){
        if(n < 0){
          n = 0;
          WriteFile(fh,&n,sizeof(DWORD),&bW,&ov);
        }
        position.Quadpart += sizeof(DWORD);
        //Advance to next record
        ov.Offset = position.LowPart;
        ov.OffsetHigh = position.HighPart;
      }
    }
